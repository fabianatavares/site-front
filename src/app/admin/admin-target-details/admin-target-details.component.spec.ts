import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminTargetDetailsComponent } from './admin-target-details.component';

describe('AdminTargetDetailsComponent', () => {
  let component: AdminTargetDetailsComponent;
  let fixture: ComponentFixture<AdminTargetDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminTargetDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminTargetDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
