import { Component, OnInit, Input, Inject } from '@angular/core';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { Location } from '@angular/common';
import { Target } from './../classes/target.model';
import { AdminTargetService } from '../services/admin-target.service';
import { FormGroup, FormBuilder, Validators, NgForm } from '@angular/forms';
import {MatTableDataSource, MatPaginator, MatSort, MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';


@Component({
  selector: 'app-admin-target-details',
  templateUrl: './admin-target-details.component.html',
  styleUrls: ['./admin-target-details.component.css']
})
export class AdminTargetDetailsComponent implements OnInit {
  @Input() targets: Target;

  id: number;
  titulo: string;
  descricao: string;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private targetService: AdminTargetService,
    private location: Location,
    public dialog: MatDialog
  ) { }

  ngOnInit(): void {
    this.getTargetDetails();
  }

  getTargetDetails(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.targetService.getTargetDetails(id)
      .subscribe(targets => {
        this.targets = targets;
    }); 
  }

  goBack(): void {
    this.location.back();
  }

  save(form: NgForm) {
    console.log(form);
    const id = +this.route.snapshot.paramMap.get('id');
    this.targetService.updateTarget(id, form).subscribe(result => {
      this.goBack();
    }, error => console.error(error));
  }

  deleteDialog(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.targetService.getTargetDetails(id)
      .subscribe(targets => {
        this.targets = targets;
    }); 

    let matDialogRef = this.dialog.open(DeleteConfirmDialogComponent, {
      height: '400px',
      width: '600px',
      data: this.targets
    });

    matDialogRef.afterClosed().subscribe(result => {
     // console.log('The dialog was closed');
      this.targets = result;
    });
  }
}


/*
* Delete new Target, using component Dialog Angular Material
* 06/06/2018 - Fabiana Tavares
*/
@Component({
  // changeDetection: ChangeDetectionStrategy.OnPush,
   selector: 'dialog-delete-target',
   templateUrl: './dialog-delete-target.component.html',
   styleUrls: ['./admin-target-details.component.css']
})
export class DeleteConfirmDialogComponent {
 
  constructor(
    private location: Location,
    private targetService: AdminTargetService, 
    public dialogRef: MatDialogRef<DeleteConfirmDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public  targets: Target) { }
 
  closeDelete(id): void {
    this.dialogRef.close();
  }

  confirmDelete(id) {
    this.targetService.deleteTarget(id).subscribe(result => {
      this.goBack();
    }, error => console.error(error));
  }  

  goBack(): void {
    this.location.back();
  }
 
}
 
