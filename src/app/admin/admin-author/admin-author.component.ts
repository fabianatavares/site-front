import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AdminAuthorService } from './admin-author.service';
import { AdminComponent } from './../admin.component';
import { AdminModule } from './../admin.module';
import { switchMap } from 'rxjs/operators';

@Component({ 
  selector: 'app-admin-author',
  templateUrl: './admin-author.component.html',
  styleUrls: ['./admin-author.component.css'],
  providers: [AdminAuthorService]
})
export class AdminAuthorComponent implements OnInit {

  authors: any[] = [];
  title = 'Chat API';

  constructor(){}

  ngOnInit() {
  
  }

}
