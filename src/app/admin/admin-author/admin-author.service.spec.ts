import { TestBed, inject } from '@angular/core/testing';

import { AdminAuthorService } from './admin-author.service';

describe('AdminAuthorService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AdminAuthorService]
    });
  });

  it('should be created', inject([AdminAuthorService], (service: AdminAuthorService) => {
    expect(service).toBeTruthy();
  }));
});
