import { Component, ViewChild, OnInit, Inject } from '@angular/core';
import {MatTableDataSource, MatPaginator, MatSort, MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { Target } from './../classes/target.model';
import { AdminTargetService } from '../services/admin-target.service';
import { FormGroup, FormBuilder, Validators, NgForm } from '@angular/forms';

@Component({
  selector: 'app-admin-target',
  templateUrl: './admin-target.component.html',
  styleUrls: ['./admin-target.component.css']
})
export class AdminTargetComponent implements OnInit {

  //displayedColumns = ['id', 'titulo', 'descricao'];
  displayedColumns = ['id', 'titulo', 'actions'];
  dataSource = new MatTableDataSource();

  targets: Target[] = [];

  titulo: string;
  descricao: string;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private targetService: AdminTargetService, 
    public dialog: MatDialog) { }

  ngOnInit() {
    this.getTarget();    
  }

  getTarget(): void {
    this.targetService.getTarget().subscribe(targets => {
      this.targets = targets;  
      this.dataSource.data = this.targets;
      this.dataSource.paginator = this.paginator;  
    });  
  } 

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  } 

  openDialog(): void {
    let matDialogRef = this.dialog.open(DialogOverviewExampleDialog, {
      height: '400px',
      width: '600px',
      data: { titulo: this.titulo, descricao: this.descricao }
    });

    matDialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      this.titulo = result;
    });
  }

  
}

/*
* ADD new Target, using component Dialog Angular Material
* 06/06/2018 - Fabiana Tavares
*/
@Component({
  selector: 'dialog-add-target',
  templateUrl: './dialog-add-target.component.html',
  styleUrls: ['./admin-target.component.css']
})
export class DialogOverviewExampleDialog {

  constructor(
    private targetService: AdminTargetService, 
    public dialogRef: MatDialogRef<DialogOverviewExampleDialog>,
    @Inject(MAT_DIALOG_DATA) public multiInput: any) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

  confirmAdd(form: NgForm) {
    this.targetService.addTarget(form).subscribe(result =>{
      this.onNoClick();
    }, error => console.error(error));
  }

}

