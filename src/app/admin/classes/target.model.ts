export class Target {

    constructor(
        public id: number,
        public titulo: string,
        public descricao: string
    ){}
}