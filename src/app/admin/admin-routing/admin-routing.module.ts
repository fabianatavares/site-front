import { NgModule, Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './../../guards/auth.guard';
import { AdminComponent } from './../admin.component';
import { AdminDashboard1Component } from './../admin-dashboard1/admin-dashboard1.component';
import { ProfileComponent } from './../../auth/profile/profile.component';
import { AdminAuthorComponent } from './../admin-author/admin-author.component';
import { AdminContentComponent } from './../admin-content/admin-content.component';
import { AdminTargetComponent } from './../admin-target/admin-target.component';
import { AdminTargetDetailsComponent } from './../admin-target-details/admin-target-details.component';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'admin',
        component: AdminComponent, canActivate: [AuthGuard], canActivateChild: [AuthGuard],
        children: [
          {
            path: '',
            redirectTo: 'dashboard',
            pathMatch: 'full'
          },
          { path: 'dashboard', component: AdminDashboard1Component},
          { path: 'author', component: AdminAuthorComponent },
          { path: 'content', component: AdminContentComponent },
          { path: 'target', component: AdminTargetComponent },
          { path: 'target/:id', component: AdminTargetDetailsComponent },
          {path: '**', redirectTo: '/404'}
        ],
      }
      
    ])
  ],
  exports: [
    RouterModule
  ]
})
export class AdminRoutingModule { }
