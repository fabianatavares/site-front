import { TestBed, inject } from '@angular/core/testing';

import { AdminTargetService } from './admin-target.service';

describe('AdminTargetService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AdminTargetService]
    });
  });

  it('should be created', inject([AdminTargetService], (service: AdminTargetService) => {
    expect(service).toBeTruthy();
  }));
});
