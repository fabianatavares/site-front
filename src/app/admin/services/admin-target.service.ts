import { Router } from '@angular/router';
import { Injectable, ErrorHandler, Injector } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { environment } from './../../../environments/environment';
import { Observable, of, throwError } from 'rxjs';
import { catchError, map, tap, retry } from 'rxjs/operators';

import { Target } from './../classes/target.model';
import { AplicationErrorHandle } from './../../app.error-handle';

const token = localStorage.getItem('token');
const httpOptions = {
  headers: new HttpHeaders({'Authorization': `Bearer ${token}`, 
                            'Content-Type': 'application/json', 'Response-Type': 'text' })
};

@Injectable({
  providedIn: 'root'
})
export class AdminTargetService {
  constructor(private http: HttpClient, private router: Router) { }

  /** GET Target from the server */
  getTarget(): Observable<Target[]> {
     return this.http.get<Target[]>(`${environment.api_url}/admin/target`)
       .pipe(
          tap(data => console.log('fetched heroes', data)),
          catchError(this.handleError('getTarget', []))
        ); 
  }

  /** GET hero by id. Will 404 if id not found */
  getTargetDetails(id: number): Observable<Target> {
    const url = `${environment.api_url}/admin/target/${id}`;
    return this.http.get<Target>(url)
    .pipe(
      tap(data => console.log(`fetched TARGETSSS id=${id}`, data)),
      catchError(this.handleError<Target>(`getTarget id=${id}`))
    );
  }

  /** PUT: update the hero on the server */
  updateTarget(id: number, targets: any): Observable<any> {
    var body = JSON.stringify(targets);    
    return this.http.post(`${environment.api_url}/admin/target/${id}`, body, httpOptions)
    .pipe(
      tap(data => console.log(`updated targets id=${id}`, data)),
      catchError(this.handleError<Target>('updateHero'))
    );
  }

  /** POST: add a new hero to the server */
  addTarget(target: any): Observable<any> {
    return this.http.post(`${environment.api_url}/admin/target`, target, httpOptions)
    .pipe(
      tap((target: Target) => console.log(`added hero w/ id=${target.id}`, target)),
      catchError(this.handleError<Target>('addHero'))
    );
  }

  deleteTarget(target: any): Observable<Target> {
    var body = JSON.stringify(target); 
    console.log(body)
    
    const id = typeof target === 'number' ? target : target.id;
    const url = `${environment.api_url}/admin/target/${id}`;

    return this.http.delete<Target>(url, httpOptions)
    .pipe(
      tap(data => console.log(`deleted hero id=${id}`, data)),
      catchError(this.handleError<Target>('deleteHero'))
    );
  }


  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
 
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
 
      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);
 
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  /** Log a HeroService message with the MessageService */
  private log(message: string) {
    //this.messageService.add('HeroService: ' + message);

  }
 
  

}
